﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using simpleCRUD.Interface;
using simpleCRUD.Models.SQLServer;
using simpleCRUD.Tools;
using simpleCRUD.ViewModels;

namespace simpleCRUD.Repositories
{
    public class RUser : IUser
    {
        private readonly DBTestContext _context;
        
        public RUser(DBTestContext context)
        {
            _context = context;
        }

        #region CREATE
        public async Task<ServiceResponseSingleVM> Create(User req)
        {
            ServiceResponseSingleVM response = new ServiceResponseSingleVM();
            try
            {
                if(req == null)
                {
                    response = new ServiceResponseSingleVM()
                    {
                        Message = "Model Kosong",
                        Code = -2,
                        Status = false
                    };
                }
                else
                {
                    _context.Users.Add(req);
                    _context.SaveChanges();
                    response = new ServiceResponseSingleVM()
                    {
                        Message = "Sukses",
                        Code = 1,
                        Status = true
                    };
                }
            }
            catch (Exception ex)
            {

                response = new ServiceResponseSingleVM()
                {
                    Message = ex.Message,
                    Code = -1,
                    Status = false
                };
            }
            return response;
        }
        #endregion

        #region DELETE
        public async Task<ServiceResponseSingleVM> Delete(idUserVM req)
        {
            ServiceResponseSingleVM response = new ServiceResponseSingleVM();
            try
            {
                User deluser = await _context.Users.Where(x => x.Id == req.Id).FirstOrDefaultAsync().ConfigureAwait(false);
                if(deluser != null)
                {
                    deluser.IsDeleted = true;
                    _context.Entry(deluser).State = EntityState.Modified;
                    await _context.SaveChangesAsync().ConfigureAwait(false);
                    response = new ServiceResponseSingleVM()
                    {
                        Message = "Sukses",
                        Code = 1,
                        Status = true
                    };
                }
                else
                {
                    response = new ServiceResponseSingleVM()
                    {
                        Message = "Id tidak ada didalam DB",
                        Code = -2,
                        Status = false
                    };
                }
            }
            catch(Exception ex)
            {
                response = new ServiceResponseSingleVM()
                {
                    Message = ex.Message,
                    Code = -1,
                    Status = false
                };
            }
            return response;
        }
        #endregion

        #region UPDATE

        public async Task<ServiceResponseSingleVM> Update(User req)
        {
            ServiceResponseSingleVM response = new ServiceResponseSingleVM();
            try
            {
                if (req == null)
                {
                    response = new ServiceResponseSingleVM()
                    {
                        Message = "Model Kosong",
                        Code = -2,
                        Status = false
                    };
                }
                else
                {
                    User data = await _context.Users.Where(x => x.Id == req.Id).FirstOrDefaultAsync().ConfigureAwait(false);
                    if(data == null)
                    {
                        response = new ServiceResponseSingleVM()
                        {
                            Message = "Id Salah",
                            Code = -2,
                            Status = false
                        };
                    }
                    else
                    {
                        data.IsDeleted = req.IsDeleted;
                        data.Email = req.Email;
                        data.Gender = req.Gender;
                        data.Name = req.Name;
                        data.IsActive = req.IsActive;
                        _context.Entry(data).State = EntityState.Modified;
                        _context.SaveChanges();
                        response = new ServiceResponseSingleVM()
                        {
                            Message = "Sukses",
                            Code = 1,
                            Status = true
                        };
                    }

                  
                }
            }
            catch (Exception ex)
            {

                response = new ServiceResponseSingleVM()
                {
                    Message = ex.Message,
                    Code = -1,
                    Status = false
                };
            }
            return response;
        }
        #endregion


        #region Load Table
        public async Task<(bool status, string error, List<LoadUserVM> trans)> LoadUser(reqLoadTableVM req)
        {
            List<LoadUserVM> list = new List<LoadUserVM>();
            int recordsTotal = 0;

            list = StoredProcedureExecutor.ExecuteSPList<LoadUserVM>(_context, "[SP_User_View]", new SqlParameter[]{
                        new SqlParameter("@PageNumber", req.PageNumber),
                        new SqlParameter("@RowsPage", req.PageSize)
                       });
            recordsTotal = StoredProcedureExecutor.ExecuteScalarInt(_context, "[SP_User_Count]", new SqlParameter[]{
                });
            if (list == null)
            {
                list = new List<LoadUserVM>();
                recordsTotal = 0;
            }
            return (true, "", new List<LoadUserVM>());
        }

        #endregion
    }
}
