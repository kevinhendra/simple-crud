using Microsoft.EntityFrameworkCore;
using simpleCRUD.Interface;
using simpleCRUD.Models.SQLServer;
using simpleCRUD.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddScoped<IUser, RUser>();

//Get DB
builder.Services.AddDbContext<DBTestContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("dbTest"));
});

//add cors
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll",
        builder =>
        builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("AllowAll");
app.UseAuthorization();

app.MapControllers();

app.Run();
