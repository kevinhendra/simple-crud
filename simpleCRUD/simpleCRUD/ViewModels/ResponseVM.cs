﻿namespace simpleCRUD.ViewModels
{
    public class ServiceResponse<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public List<T> Data { get; set; }
    }

    public class ServiceResponseSingleVM<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class ServiceResponseSingleVM
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
