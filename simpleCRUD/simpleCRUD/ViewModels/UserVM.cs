﻿namespace simpleCRUD.ViewModels
{
    public class LoadUserVM
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public bool isDeleted { get; set; }
    }

    public class reqLoadTableVM
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class idUserVM
    {
        public int Id { get; set; }
    }
}
