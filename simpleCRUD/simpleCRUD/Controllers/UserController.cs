﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using simpleCRUD.Interface;
using simpleCRUD.Models.SQLServer;
using simpleCRUD.ViewModels;

namespace simpleCRUD.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IUser _user;
        public UserController(IUser user)
        {
            _user = user;
        }

        #region LoadData
        [DisableCors]
        [HttpPost]
        public async Task<IActionResult> LoadData(reqLoadTableVM req)
        {
            var res = new ServiceResponse<LoadUserVM>();
            try
            {
                var _ = await _user.LoadUser(req);
                if (!_.status)
                {
                    res.Code = -2;
                    res.Message = _.error;
                }
                else
                {
                    res.Code = 1;
                    res.Message = _.error;
                    res.Data = _.trans;
                }
            }
            catch (Exception ex)
            {
                res.Code = -1;
                res.Message = ex.Message == null ? ex.InnerException.ToString() : ex.Message.ToString();

            }

            return new OkObjectResult(res);
        }
        #endregion

        #region DELETE
        [DisableCors]
        [HttpPost]
        public async Task<IActionResult> DeleteFile([FromBody] idUserVM req)
        {
            var res = new ServiceResponseSingleVM<string>();
            try
            {
                var _ = await _user.Delete(req);
                if (!_.Status)
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
                else
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
            }
            catch (Exception ex)
            {
                res.Code = -1;
                res.Message = ex.Message == null ? ex.InnerException.ToString() : ex.Message.ToString();
            }
            return new OkObjectResult(res);
        }



        #endregion

        #region Update
        [DisableCors]
        [HttpPost]
        public async Task<IActionResult> Update([FromBody] User req)
        {
            var res = new ServiceResponseSingleVM<string>();
            try
            {
                var _ = await _user.Update(req);
                if (!_.Status)
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
                else
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
            }
            catch (Exception ex)
            {
                res.Code = -1;
                res.Message = ex.Message == null ? ex.InnerException.ToString() : ex.Message.ToString();
            }
            return new OkObjectResult(res);
        }



        #endregion

        #region CRERATE
        [DisableCors]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] User req)
        {
            var res = new ServiceResponseSingleVM<string>();
            try
            {
                var _ = await _user.Create(req);
                if (!_.Status)
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
                else
                {
                    res.Code = _.Code;
                    res.Message = _.Message;
                }
            }
            catch (Exception ex)
            {
                res.Code = -1;
                res.Message = ex.Message == null ? ex.InnerException.ToString() : ex.Message.ToString();
            }
            return new OkObjectResult(res);
        }



        #endregion


    }
}
