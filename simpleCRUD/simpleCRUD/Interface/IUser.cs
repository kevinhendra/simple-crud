﻿using simpleCRUD.Models.SQLServer;
using simpleCRUD.ViewModels;

namespace simpleCRUD.Interface
{
    public interface IUser
    {
        Task<(bool status, string error, List<LoadUserVM> trans)> LoadUser(reqLoadTableVM req);

        Task<ServiceResponseSingleVM> Delete(idUserVM req);
        Task<ServiceResponseSingleVM> Create(User req);
        Task<ServiceResponseSingleVM> Update(User req);

    }
}
